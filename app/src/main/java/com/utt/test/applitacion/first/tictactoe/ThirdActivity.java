package com.utt.test.applitacion.first.tictactoe;

import android.content.Intent;
import android.os.Process;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

public class ThirdActivity extends AppCompatActivity {

    private Intent ini;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);
        savedInstanceState=getIntent().getExtras();
        int r=savedInstanceState.getInt("result");
        switch (r){
            case R.drawable.circulo:
                ((ImageView)findViewById(R.id.imv_result)).setImageResource(R.drawable.circulo);
                break;
            case R.drawable.equis:
                ((ImageView)findViewById(R.id.imv_result)).setImageResource(R.drawable.equis);
                break;
            case -1:
                ((ImageView)findViewById(R.id.imv_result)).setImageResource(R.drawable.zzz);
                break;
        }
        ((Button)findViewById(R.id.btn_restart)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ini=new Intent(ThirdActivity.this,MainActivity.class);
                startActivity(ini);
                finish();
            }
        });

        ((Button)findViewById(R.id.btn_exit)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Process.killProcess(Process.myPid());
            }
        });
    }
}
