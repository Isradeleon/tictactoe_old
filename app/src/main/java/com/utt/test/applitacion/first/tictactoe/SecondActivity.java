package com.utt.test.applitacion.first.tictactoe;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class SecondActivity extends AppCompatActivity {

    int turn;
    ArrayList<ImageView> the_views;
    ArrayList<String> indexes;
    int[] the_data;
    int winner;
    private Intent inin;
    int counter;
    Random r;
    CountDownTimer cc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        savedInstanceState=getIntent().getExtras();
        this.turn=savedInstanceState.getInt("turn");
        the_data=new int[]{-1,-1,-1,-1,-1,-1,-1,-1,-1};
        this.winner=-1;
        this.counter=6;
        r=new Random();

        cc=new CountDownTimer(6000,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                counter-=1;
                ((TextView)findViewById(R.id.tv_timer)).setText(String.valueOf(counter));
            }

            @Override
            public void onFinish() {
                int ran=getRandom();
                switch (turn){
                    case R.id.iview_circle:
                        turn=R.id.iview_cross;
                        the_views.get(ran).setImageResource(R.drawable.circulo);
                        the_views.get(ran).setEnabled(false);
                        ((ImageView)findViewById(R.id.iview_indicator)).setImageResource(R.drawable.equis);
                        the_data[ran]=R.drawable.circulo;
                        indexes.remove(String.valueOf(ran));

                        break;
                    case R.id.iview_cross:
                        turn=R.id.iview_circle;
                        the_views.get(ran).setImageResource(R.drawable.equis);
                        the_views.get(ran).setEnabled(false);
                        ((ImageView)findViewById(R.id.iview_indicator)).setImageResource(R.drawable.circulo);
                        the_data[ran]=R.drawable.equis;
                        indexes.remove(String.valueOf(ran));

                        break;
                }
                winner=checkWinner();
                if(winner != -1){
                    inin=new Intent(SecondActivity.this,ThirdActivity.class);
                    inin.putExtra("result",winner);
                    startActivity(inin);
                    methodIKnow();
                }else if(checkEqualGame()){
                    inin=new Intent(SecondActivity.this,ThirdActivity.class);
                    inin.putExtra("result",winner);
                    startActivity(inin);
                    methodIKnow();
                }else{
                    restartCountDown();
                }
            }
        };
        cc.start();

        switch (turn){
            case R.id.iview_circle:
                ((ImageView)findViewById(R.id.iview_indicator)).setImageResource(R.drawable.circulo);
                break;
            case R.id.iview_cross:
                ((ImageView)findViewById(R.id.iview_indicator)).setImageResource(R.drawable.equis);
                break;
        }

        the_views=new ArrayList<>();
        the_views.add(((ImageView)findViewById(R.id.action_1)));
        the_views.add(((ImageView)findViewById(R.id.action_2)));
        the_views.add(((ImageView)findViewById(R.id.action_3)));
        the_views.add(((ImageView)findViewById(R.id.action_4)));
        the_views.add(((ImageView)findViewById(R.id.action_5)));
        the_views.add(((ImageView)findViewById(R.id.action_6)));
        the_views.add(((ImageView)findViewById(R.id.action_7)));
        the_views.add(((ImageView)findViewById(R.id.action_8)));
        the_views.add(((ImageView)findViewById(R.id.action_9)));

        indexes=new ArrayList<>();
        indexes.add("0");
        indexes.add("1");
        indexes.add("2");
        indexes.add("3");
        indexes.add("4");
        indexes.add("5");
        indexes.add("6");
        indexes.add("7");
        indexes.add("8");

        for(ImageView imv : the_views){
            imv.setImageResource(R.drawable.theres_no_image);
        }
    }

    private void methodIKnow(){
        cc.cancel();
        finish();
    }

    private void restartCountDown() {
        cc.cancel();
        this.counter=6;
        cc.start();
    }

    private int getRandom(){
        return Integer.valueOf(indexes.get(r.nextInt(indexes.size())));
    }

    public void eventPlayer(View v){
        switch (turn){
            case R.id.iview_circle:
                this.turn=R.id.iview_cross;
                ((ImageView)v).setImageResource(R.drawable.circulo);
                ((ImageView)v).setEnabled(false);
                ((ImageView)findViewById(R.id.iview_indicator)).setImageResource(R.drawable.equis);
                the_data[the_views.indexOf(((ImageView)v))]=R.drawable.circulo;
                indexes.remove(String.valueOf(the_views.indexOf(((ImageView)v))));

                break;
            case R.id.iview_cross:
                this.turn=R.id.iview_circle;
                ((ImageView)v).setImageResource(R.drawable.equis);
                ((ImageView)v).setEnabled(false);
                ((ImageView)findViewById(R.id.iview_indicator)).setImageResource(R.drawable.circulo);
                the_data[the_views.indexOf(((ImageView)v))]=R.drawable.equis;
                indexes.remove(String.valueOf(the_views.indexOf(((ImageView)v))));

                break;
        }

        this.winner=checkWinner();
        if(this.winner != -1){
            inin=new Intent(this,ThirdActivity.class);
            inin.putExtra("result",this.winner);
            startActivity(inin);

            cc.cancel();
            finish();
        }else if(checkEqualGame()){
            inin=new Intent(this,ThirdActivity.class);
            inin.putExtra("result",this.winner);
            startActivity(inin);

            cc.cancel();
            finish();
        }else{
            restartCountDown();
        }
    }

    private boolean checkEqualGame(){
        int cont=0;
        for(int imv : the_data){
            if(imv!=-1){
                cont++;
            }
        }

        if(cont==the_data.length){
            return true;
        }else{
            return false;
        }
    }

    private int checkWinner(){
        if(isValid(the_data[0],the_data[1],the_data[2]) && isTheSame(the_data[0],the_data[1],the_data[2])){
            return the_data[0];
        }else if(isValid(the_data[3],the_data[4],the_data[5]) && isTheSame(the_data[3],the_data[4],the_data[5])){
            return the_data[3];
        }else if(isValid(the_data[6],the_data[7],the_data[8]) && isTheSame(the_data[6],the_data[7],the_data[8])){
            return the_data[6];
        }else if(isValid(the_data[0],the_data[3],the_data[6]) && isTheSame(the_data[0],the_data[3],the_data[6])){
            return the_data[0];
        }else if(isValid(the_data[1],the_data[4],the_data[7]) && isTheSame(the_data[1],the_data[4],the_data[7])){
            return the_data[1];
        }else if(isValid(the_data[2],the_data[5],the_data[8]) && isTheSame(the_data[2],the_data[5],the_data[8])){
            return the_data[2];
        }else if(isValid(the_data[0],the_data[4],the_data[8]) && isTheSame(the_data[0],the_data[4],the_data[8])){
            return the_data[0];
        }else if(isValid(the_data[6],the_data[4],the_data[2]) && isTheSame(the_data[6],the_data[4],the_data[2])){
            return the_data[6];
        }else{
            return -1;
        }
    }

    private boolean isValid(int a, int b, int c){
        if(a != -1 && b != -1 && c != -1){
            return true;
        }else{
            return false;
        }
    }

    private boolean isTheSame(int a, int b, int c){
        if(a == b && c == b){
            return true;
        }else{
            return false;
        }
    }
}
