package com.utt.test.applitacion.first.tictactoe;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private Intent in;
    private int turn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        turn=R.id.iview_circle;
        ((Button) findViewById(R.id.btn_start)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                in=new Intent(MainActivity.this,SecondActivity.class);
                in.putExtra("turn",turn);
                startActivity(in);
                finish();
            }
        });
    }

    public void clickImageView(View v){
        switch (((ImageView)v).getId()){
            case R.id.iview_circle:
                this.turn=R.id.iview_circle;
                ((ImageView)v).setImageResource(R.drawable.select1);
                ((ImageView)findViewById(R.id.iview_cross)).setImageResource(R.drawable.equis);
                break;
            case R.id.iview_cross:
                this.turn=R.id.iview_cross;
                ((ImageView)v).setImageResource(R.drawable.select2);
                ((ImageView)findViewById(R.id.iview_circle)).setImageResource(R.drawable.circulo);
                break;
        }
    }
}
